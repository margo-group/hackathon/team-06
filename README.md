# Team-06

Idée : L'utilisateur saisi les informations sur une interface utlisateur.
Les informations sont envoyées dans une base de donnée ElasticSearch et cryptées.
Le HashCode est ensuite stocké dans la blockchain. 

Le dossier B4Y est une ébauche de projet utilisant Truffle, que l'on a pas réussi à connecter à la BlockChain Skillz.
Le dossier src contient néanmoins l'interface utilisateur.

Le fichier DocHash.sol contient le smart contract que nous avons deployé sur la blockchain Skillz.
Le résumé de la transaction se trouve dans DocHashDeploy.txt.

Le projet HackathonBC est un projet C# qui s'occupe de récupérer un JSON contenant les infos de l'utilisateur,
les crypter et les envoyer dans la BDD ElasticSearch.

