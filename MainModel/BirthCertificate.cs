﻿using Nest;
using System;
using System.IO;
using System.Linq;
using System.Text;


namespace MainModel
{
    public class BirthCertificate
    {
        public Person Applicant { get; private set; }

        public Person Father { get; private set; }

        public Person Mother { get; private set; }

        public string DateOfBirth { get; private set; }


        public string PlaceOfBirth { get; private set; }

        public string Gender { get; private set; }

        public string ApplicantPublicKey { get; private set; }

        private readonly string rootPath;

        private readonly string fileName;

        private CryptographyHelper crypto;
        public byte[] Hash { get; private set; }

        internal string DataString { get; set; }

        private DocInfo info;

        public BirthCertificate(Person applicant, Person mother, Person father, DateTime dateOfBirth, string placeOfBirth, Gender gender, string rootPath = "")
        {
            crypto = new CryptographyHelper(rootPath + "publicKey.txt", rootPath + "privateKey.txt");
            ApplicantPublicKey = File.ReadAllText(rootPath + "publicKey.txt");

            this.Applicant = applicant;
            this.Mother = mother;
            this.Father = father;
            this.DateOfBirth = dateOfBirth.ToString();
            this.PlaceOfBirth = placeOfBirth.ToString();

            if (gender == MainModel.Gender.Female)
            {
                this.Gender = "Female";
            }
            else
            {
                this.Gender = "Male";
            }

            this.rootPath = rootPath;
        }

        //constructor only for ES search result
        public BirthCertificate(Person applicant, Person mother, Person father, string dateOfBirth, string placeOfBirth, string gender)
        {
            this.Applicant = applicant;
            this.Mother = mother;
            this.Father = father;
            this.DateOfBirth = dateOfBirth;
            this.PlaceOfBirth = placeOfBirth;
            this.Gender = gender;

        }

        public bool PushDocument(bool push = true)
        {
            SetHash();
            EncryptData();
            if (push)
            {
                return DbProvider.Push(this);

            }
            else
            {
                return true;
            }
        }

        public bool GetDocument(out BirthCertificate outDoc)
        {
            if (Pull(ApplicantPublicKey, out BirthCertificate bc))
            {
                bc.DecryptData(this.crypto);
                outDoc = bc;
                return true;
            }
            else
            {
                outDoc = null;
                return false;
            }
        }

        public bool RetrieveDoc()
        {
            GetDocument(out BirthCertificate bc);
            if (bc != null && Compare(bc))
            {
                Misc.BcToJson(rootPath + "doc.json", bc);
            }

            return true;
        }

        public void SetHash()
        {
            DataString = StringifyInfo();
            HashProvider.GetHash(DataString, out byte[] hashValue);
            this.Hash = hashValue;
        }

        public bool Compare(BirthCertificate otherBc)
        {
            //hash = Convert.FromBase64String(hashValue);
            otherBc.SetHash();
            return Convert.ToBase64String(this.Hash) == Convert.ToBase64String(otherBc.Hash);
        }

        private string StringifyInfo()
        {
            return Applicant.Name + Applicant.Surname + Mother.Name + Mother.Surname + Father.Name + Father.Surname +
             DateOfBirth + PlaceOfBirth + Gender;
        }

        public void CreateDocInfo()
        {
            this.info = new DocInfo("BirthCertificate"+"_53585494198", Convert.ToBase64String(this.Hash), rootPath + "info.dat");
            Misc.DocInfoToJson(rootPath + "info.dat", info);
        }

        private void EncryptData()
        {
            this.Applicant = new Person(crypto.Encrypt(Applicant.Name), crypto.Encrypt(Applicant.Surname));
            this.Mother = new Person(crypto.Encrypt(Mother.Name), crypto.Encrypt(Mother.Surname));
            this.Father = new Person(crypto.Encrypt(Father.Name), crypto.Encrypt(Father.Surname));
            this.DateOfBirth = crypto.Encrypt(DateOfBirth.ToString());
            this.PlaceOfBirth = crypto.Encrypt(PlaceOfBirth.ToString());
            this.Gender = crypto.Encrypt(Gender);
        }

        private void DecryptData(CryptographyHelper ch)
        {
            if (this.crypto == null)
            {
                this.crypto = ch;
            }

            this.Applicant = new Person(crypto.Decrypt(Applicant.Name), crypto.Decrypt(Applicant.Surname));
            this.Mother = new Person(crypto.Decrypt(Mother.Name), crypto.Decrypt(Mother.Surname));
            this.Father = new Person(crypto.Decrypt(Father.Name), crypto.Decrypt(Father.Surname));
            this.DateOfBirth = crypto.Decrypt(DateOfBirth.ToString());
            this.PlaceOfBirth = crypto.Decrypt(PlaceOfBirth.ToString());
            this.Gender = crypto.Decrypt(Gender);
        }

        public bool Pull(string mathchinPattern, out BirthCertificate bc)
        {
            bc = null;
            var settings = new ConnectionSettings(new Uri("http://localhost:9200")).DefaultIndex(DbProvider.indexName);
            settings.EnableDebugMode();
            ElasticClient client = new ElasticClient(settings);
            var searchResponse = client.Search<BirthCertificate>(s => s
                .From(0)
                .Size(1)
                .Query(q => q
                     .Match(m => m
                        .Field(f => ApplicantPublicKey)
                        .Query(mathchinPattern)
                     )
                )
            );
            if (searchResponse.Documents.Count > 0)
            {
                bc = searchResponse.Documents.ElementAt(0);
            }

            return bc != null;
        }

        public void BlockChainCallBack(string commitHash)
        {
            this.info.Update(commitHash);
            Misc.DocInfoToJson(rootPath + "info.dat", info);
        }
    }
}
