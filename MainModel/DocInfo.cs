﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;


namespace MainModel
{
    public class DocInfo
    {
        public string Hash { get; set; }

        public string DocumentName { get; set; }

        public string BlockHash { get; set; }

        public DateTime CreationDate { get; set; }

        public DocumentStatus Status { get; private set; }

        private string fileName;

        public DocInfo(string name, string hash, string fileName)
        {
            this.DocumentName = name;
            this.Hash = hash;

            CreationDate = DateTime.Now;
            BlockHash = "";
            SetStatus();
            this.fileName = fileName;
        }

        private void SetStatus()
        {
            if (string.IsNullOrEmpty(BlockHash))
            {
                Status = DocumentStatus.Validated;
            }
            else
            {
                Status = DocumentStatus.Pending;
            }
        }


        public void Update(string commitHash)
        {
            this.BlockHash = commitHash;
            SetStatus();
        }
    }

    public enum DocumentStatus
    {
        Pending,
        Validated
    }
}
