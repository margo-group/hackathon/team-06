﻿using System.IO;
using System.Text;
using System.Text.Json;

namespace MainModel
{
    class Misc
    {
        // Display the byte array in a readable format.
        private static string PrintByteArray(byte[] array)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                sb.Append($"{array[i]:x2}");
            }
            return sb.ToString();
        }

        public static void DocInfoToJson(string filePath, DocInfo info)
        {
            File.WriteAllText(filePath, JsonSerializer.Serialize<DocInfo>(info));
        }

        public static void BcToJson(string filePath, BirthCertificate info)
        {
            File.WriteAllText(filePath, JsonSerializer.Serialize<BirthCertificate>(info));
        }
    }
}
