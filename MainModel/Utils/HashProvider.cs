﻿using System;
using System.Security.Cryptography;


namespace MainModel
{
    public static class HashProvider
    {
        public static bool GetHash(string dataString, out byte[] hashValue)
        {
            byte[] data = System.Text.ASCIIEncoding.UTF8.GetBytes(dataString);

            using (SHA256 mySHA256 = SHA256.Create())
            {
                try
                {
                    hashValue = mySHA256.ComputeHash(data);
                }
                catch
                {
                    hashValue = null;
                    return false;
                }
                return true;
            }
        }

        public static bool GetHash(byte[] data, out byte[] hashValue)
        {
            using (SHA256 mySHA256 = SHA256.Create())
            {
                try
                {
                    hashValue = mySHA256.ComputeHash(data);
                }
                catch
                {
                    hashValue = null;
                    return false;
                }
                return true;
            }
        }
    }
}
