App = {
  web3Provider: null,
  contracts: {},

  init: async function() {
    // Load pets.
    /*$.getJSON('../pets.json', function(data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i ++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);

        petsRow.append(petTemplate.html());
      }
    });*/

    return await App.initWeb3();
  },

  initWeb3: async function() {
    if (typeof web3 !== 'undefined') {
		App.web3Provider = web3.currentProvider;
	} else {
		// Si aucune connexion web3 n'existe, nous allons nous connecter à Ganache
		App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
	}
	web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('DocHash.json', function(data) {
	// On récupère les données bytecode du contrat
	var DocHashArtifact = data;
	App.contracts.DocHash = TruffleContract(DocHashArtifact);
 
	// On utilise le provider définit dans la section précédente
	App.contracts.DocHash.setProvider(App.web3Provider);
 
	// On vérifie si un animal est déjà adopté
	return App.markAdopted();
	});

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
  },

  markAdopted: function(adopters, account) {
    var docHashInstance;
 
	web3.eth.getAccounts(function(error, accounts) {
	if (error) {
	console.log(error);
	}
	 
	var account = accounts[0];
	 
	App.contracts.DocHash.deployed().then(function(instance) {
	docHashInstance = instance;
	 
	// Execute adopt as a transaction by sending account
	return docHashInstance.adopt(docHash, {from: account});
	}).then(function(result) {
	return App.markAdopted();
	}).catch(function(err) {
	console.log(err.message);
	});
	});
  },

  handleAdopt: function(event) {
    event.preventDefault();

    var petId = "testeste";

    var adoptionInstance;
 
	web3.eth.getAccounts(function(error, accounts) {
	if (error) {
	console.log(error);
	}
	 
	var account = accounts[0];
	 
	App.contracts.DocHash.deployed().then(function(instance) {
	adoptionInstance = instance;
	 
	// Execute adopt as a transaction by sending account
	return adoptionInstance.adopt(petId, {from: account});
	}).then(function(result) {
	return App.markAdopted();
	}).catch(function(err) {
	console.log(err.message);
	});
	});
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
