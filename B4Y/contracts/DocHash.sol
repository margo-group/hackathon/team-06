pragma solidity ^0.5.0;

contract DocHash {
    
    address owner;
    uint256 public nbEnregistrements;
    mapping ( uint256 => string ) private mapHash;
    
    modifier onlyOwner {
        require ( msg.sender == owner );
        _;
    }
    
    constructor (  ) public {
        owner = msg.sender;
        nbEnregistrements = 0;
    }
    
    function ajouterHash ( string memory _docHash ) public onlyOwner {
        mapHash[nbEnregistrements] = _docHash;
        nbEnregistrements ++;
    }
    
    function lireHash ( uint256 _index ) public view onlyOwner returns ( string memory _docHash ) {
        _docHash = mapHash[_index];
    }
}