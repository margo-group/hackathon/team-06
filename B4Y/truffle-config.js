module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "http://35.181.48.43",
      port: 8545,
      network_id: "*" // Match any network id
    }
  }
};
