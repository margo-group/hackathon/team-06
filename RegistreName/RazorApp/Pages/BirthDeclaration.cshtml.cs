﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MainModel;

namespace RazorApp.Pages
{
    public class BirthDeclarationModel : PageModel
    {
        public string Message { get; private set; } = "PageModel in C#";

        public void OnGet()
        {
            Message += $" Server time is { DateTime.Now }";
            BirthCertificate acte = new BirthCertificate(
                new Person("",""),
                new Person("",""),
                new Person("",""),
                DateTime.Now,
                "Paris",
                Gender.Male,
                "");
        }

        
    }
}