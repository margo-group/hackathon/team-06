﻿using System;
using MainModel;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            bool retrieve = false;
            if (args.Length > 0)
            {
                retrieve = true;
            }
            Console.WriteLine("Processing...");
            string datapath = "C:\\Users\\flore\\Desktop\\hackaton\\data\\";

            //create keys

            //Create certificate 
            BirthCertificate acte = new BirthCertificate(
               new Person("Emile", "Dupont"),
               new Person("Mummy", "Dupont"),
               new Person("Daddy", "Dupont"),
               new DateTime(2019,09,29),
               "Paris",
               Gender.Male, datapath);

            //Insert the certificate
            if (!retrieve)
            {
                acte.PushDocument();
                acte.CreateDocInfo();
            }
            else
            {
                acte.PushDocument();
                acte.CreateDocInfo();
                acte.BlockChainCallBack("0x303f0427faa63d26c344cbacbcaa9203a065ec06780573bf519ab491a2035452");
                acte.RetrieveDoc();
            }


            Console.WriteLine("\nDone.");
        }
    }
}
